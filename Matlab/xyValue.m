while true
    pause(2)

    workingDir = pwd;
    fileLocation = strcat(workingDir, '/distance.txt');
    fileInfo = dir(fileLocation);
    if (length(fileInfo) == 0)
        continue;
    end
    size = fileInfo.bytes;
    if (size == 0)
        continue;
    end
    
    [dist1 dist2 dist3] = textread('distance.txt', '%f %f %f', 'delimiter', ' ');

     B=zeros(4,3);

        B(1,1)=0; B(1,2)=0;B(1,3)=dist1(1);
        B(2,1)=0; B(2,2)=390;B(2,3)=dist2(1);
        B(3,1)=390; B(3,2)=390;B(3,3)=dist3(1);

        BN = 3;
    [x,y] = Trilateration(B, BN);
%     figure(1)
%     plot(B(:,1),B(:,2),'bo',x,y,'r*');

    drawnow;

    fName = 'coordinate.txt';         %# A file name
    fid = fopen(fName,'a');            %# Open the file
    if fid ~= -1
      fprintf(fid,'%0.2f %0.2f\n',x, y);       %# Print the string
      fprintf('%0.2f %0.2f\n',x, y);
      fclose(fid);                     %# Close the file
    end
    
    % empty file
    fid = fopen('distance.txt', 'w');
    fprintf(fid, '');
    fclose(fid);
end
  