/*
 * RSSIBasestation.java
 *
 * Receives data from the mobile spot on the distances to each of the xbees,
 * then converts this to an x y coordinate of position.
 */

package challenge5.basestation;

import com.sun.spot.io.j2me.radiogram.*;
import com.sun.spot.peripheral.ota.OTACommandServer;
import com.sun.spot.util.Utils;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.microedition.io.*;


/**
 * Reads data from the mobile spot, converts to x,y position, and saves to file
 */
public class RSSIBasestation {
    // Broadcast port on which we listen for sensor samples
    private static final int HOST_PORT = 55;
    private static final boolean DEBUG = false;
        
    private void run() throws Exception {
        RadiogramConnection rCon;
        Datagram dg;
         
        try {
            // Open up a server-side broadcast radiogram connection
            // to listen for sensor readings being sent by different SPOTs
            rCon = (RadiogramConnection) Connector.open("radiogram://:" + HOST_PORT);
            dg = rCon.newDatagram(rCon.getMaximumLength());
        } catch (Exception e) {
             System.err.println("setUp caught " + e.getMessage());
             throw e;
        }
     
//      /* If running on a different computer, this line needs to be changed */
        File file = new File("/Users/Raphy/Documents/School/Fall2013/EC544/Challenge5/Matlab/distance.txt");
        
        // if file doesnt exists, then create it, if it does, delete it then re create it 
        if (!file.exists()) 
        {
            file.createNewFile();
        }
        else
        {
            file.delete();
            file.createNewFile();
        }
        
        // Main data collection loop
        while (true) {
            try {
                // Read sensor sample received over the radio
                rCon.receive(dg);
                
                /* Parse the data. it is 6 ints in a row */
                String addr = dg.getAddress(); // read sender's Id to make sure everything               
                float sensorADist = dg.readFloat();
                float sensorBDist = dg.readFloat();
                float sensorCDist = dg.readFloat();
                
                if (DEBUG)
                {
                    System.out.println("Received the signal.\n" + 
                            sensorADist + " " + sensorBDist + " " + sensorCDist + "\n");
                }
                else
                {
                    System.out.println("Received the signal.\n" + 
                            sensorADist + " " + sensorBDist + " " + sensorCDist + "\n");
                    FileWriter fw = new FileWriter(file.getAbsoluteFile(), true);
                    BufferedWriter bw = new BufferedWriter(fw);
                    String content = sensorADist + " " + sensorBDist + " " +
                            sensorCDist + "\n";
                    bw.write(content);
                    bw.close();
                }
                
            } catch (Exception e) {
                System.err.println("Caught " + e +  " while reading sensor samples.");
                throw e;
            }
            
//            Utils.sleep(500);
        }
    }
    
    /**
     * Start up the host application.
     *
     * @param args any command line arguments
     */
    public static void main(String[] args) throws Exception {
        // register the application's name with the OTA Command server & start OTA running 
        OTACommandServer.start("SendDataDemo");

        RSSIBasestation app = new RSSIBasestation();
        app.run();
    }
}
