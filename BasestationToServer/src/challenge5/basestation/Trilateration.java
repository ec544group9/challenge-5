/*
 * This class provides a place to perform trilateration on 3 beacons
 * 
 * This class will be expanded to more beacons in the future potentially,
 * but currently the inverse function relies on D being a 2x2 matrix.
 */
package challenge5.basestation;
//import org.apache.commons.math3.linear;
/**
 *
 * @author Raphy
 */
public class Trilateration
{
    public int NBeacons;
    public double[] beaconX;
    public double[] beaconY;
    /* Global variables for the stages of computation */
    public double[][] D;
    public double[][] DT;
    public double[][] DTD;
    public double[][] DTDInv;
    public double[][] DTDInvDT;
    public double[][] B;
    
    
    public Trilateration(int N, double[] xpos, double[] ypos)
    {
        NBeacons = N;
        if (xpos.length != NBeacons)
        {
            System.out.println("Number of beacons is incompatible");
        }

        beaconX = xpos;

        if (ypos.length != NBeacons)
        {
            System.out.println("Number of beacons is incompatible");
        }

        beaconY = ypos;

        /* Initialize the array for computation.  The D array is
         * 
         * 2 * [ [x1 - x2, y1 - y2 ] [x1 - x3, y1 - y3] ]
         * 
         * And the array taking account distance is 
         * 
         * [ [ x1^2 - x2^2 + y1^2 - y2^2 + d1^2 - d2^2]  [ x1^2 - x3^2 + y1^2 - y3^2 + d1^2 - d3^2] ]
         * 
         * And this applies up to n dimensions, adding a row in each case to the matrix 
         * 
         * HOWEVER, for the special 3 Beacon case, we can use the following
         * 
         * 2 * [ [x1 - x2, y1 - y3 ] [x2 - x3, y2 - y3] ]
         * 
         * [ [ x1^2 - x2^2 + y1^2 - y2^2 + d1^2 - d2^2]  [ x1^2 - x3^2 + y1^2 - y3^2 + d1^2 - d3^2] ]
         * 
         * which we use to simplify the inverse calculation in java
         */

        D = new double[NBeacons - 1][];
        
        /* Caluclate D */
        for (int ii = 0 ; ii < NBeacons - 1 ; ii++)
        {
            D[ii] = new double[2];
            D[ii][0] = beaconX[0] - beaconX[ii + 1];
            D[ii][1] = beaconY[0] - beaconY[ii + 1];
        }
        
        DT = transpose(D);
        
        DTD = matmult(D, DT);
        
        DTDInv = inverse(DTD);
        
        /* Calculate the inverse of the DT * D */
        for (int ii = 0 ; ii < D.length ; ii++)
        {
            for (int jj = 0 ; jj < D[ii].length ; jj++);
        }
        
    }
    
    /* Matrix multiply valid for all dimensions, assumes
     * input matrices have proper dimensions
     */
    public double[][] matmult(double[][] A, double[][] B)
    {
        double[][] out = new double[A.length][];
        for (int ii = 0 ; ii < A.length ; ii++)
        {
            out[ii] = new double[B[0].length]; 
        }
        
        for (int ii = 0 ; ii < A.length ; ii++)
        {
            for (int jj = 0 ; jj < B[0].length ; jj++)
            {
                double sum = 0.0;
                for (int kk = 0 ; kk < B.length ; kk++)
                {
                    sum += A[ii][kk] * B[kk][jj];
                }
                out[ii][jj] = sum;
            }
        }
        
        return out;
    }
    
    /* Valid with any dimension input */
    public double[][] transpose(double[][] in)
    {
        double[][] out = new double[in.length][];
        for (int ii = 0 ; ii < in.length ; ii++)
        {
            out[ii] = new double[in[ii].length]; 
        }
        
        /* Calculate transpose of D */
        for (int ii = 0 ; ii < in.length ; ii++)
        {
            for (int jj = 0 ; jj < in[ii].length ; jj++)
            {
                out[ii][jj] = in[jj][ii];
            }
        }
        
        return out;
    }
    
    /* Currently only compatible with a 2x2 matrix, must be made compatible for more dimensions */
    public double[][] inverse(double[][] A)
    {
        if (A.length != 2 || A[0].length != 2)
        {
//            System.out.println("Can't run an inverse without a 2X2 matrix!")
        }
        return null;
    }
    
    public boolean setXPosBeacon(double[] xpos)
    {
        if (xpos.length != NBeacons)
        {
            System.out.println("Number of beacons is incompatible");
            return false;
        }
        
        beaconX = xpos;
        return true;
    }
    
    
    public boolean setYPosBeacon(double[] ypos)
    {
        if (ypos.length != NBeacons)
        {
            System.out.println("Number of beacons is incompatible");
            return false;
        }
        
        beaconY = ypos;
        return true;
    } 
    
    public double[] trilaterate(double x, double y)
    {
        double[] output = new double[2];
        
        /* I use the Apache Commons math matrix library for the inverses */
        return output;
    }
}
