/*
 * XbeeonSPOT.java
 *
 * Created on Apr 9, 2013 11:07:13 AM;
 */

package org.sunspotworld;

import com.sun.spot.io.j2me.radiogram.RadiogramConnection;
import com.sun.spot.peripheral.radio.RadioFactory;
import com.sun.spot.sensorboard.EDemoBoard;
import com.sun.spot.resources.Resources;
import com.sun.spot.resources.transducers.ISwitch;
import com.sun.spot.resources.transducers.ITriColorLED;
import com.sun.spot.resources.transducers.ITriColorLEDArray;
import com.sun.spot.resources.transducers.ILightSensor;
import com.sun.spot.service.BootloaderListenerService;
import com.sun.spot.util.IEEEAddress;
import com.sun.spot.util.Utils;
import java.io.IOException;
import javax.microedition.io.Connector;
import javax.microedition.io.Datagram;
import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;

/**
 * This is solution from group 9, for challenge 5.
 */
public class XbeeonSPOT extends MIDlet {

    private ITriColorLEDArray leds = (ITriColorLEDArray) Resources.lookup(ITriColorLEDArray.class);
    private EDemoBoard eDemo = EDemoBoard.getInstance();
    private ILightSensor light = (ILightSensor)Resources.lookup(ILightSensor.class);
    private static final int HOST_PORT = 55;

    protected void startApp() throws MIDletStateChangeException {
        RadiogramConnection rCon = null;
        Datagram dg = null;
        System.out.println("Hello, world");
        BootloaderListenerService.getInstance().start();   // monitor the USB (if connected) and recognize commands from host
        
        int[] rssi=new int[3];
        float[] distance=new float[3];
        
        for (int i = 0; i < leds.size(); i++) {
                        leds.getLED(i).setRGB(0, 0, 100);
                        leds.getLED(i).setOn();
                    }
        Utils.sleep(100);
        for (int i = 0; i < leds.size(); i++) {
                        leds.getLED(i).setOff(); 
                    }
        
        byte[] xbeeOne = new byte[9];
        xbeeOne[0] = (byte)0x13;
        xbeeOne[1] = (byte)0xA2;
        xbeeOne[2] = (byte)0x00;
        xbeeOne[3] = (byte)0x40;
        xbeeOne[4] = (byte)0xA0;
        xbeeOne[5] = (byte)0x3C;
        xbeeOne[6] = (byte)0xBE;
        xbeeOne[7] = (byte)0x66;
        xbeeOne[8] = (byte)0x3C;
        
        byte[] xbeeTwo = new byte[9];
        xbeeTwo[0] = (byte)0x13;
        xbeeTwo[1] = (byte)0xA2;
        xbeeTwo[2] = (byte)0x00;
        xbeeTwo[3] = (byte)0x40;
        xbeeTwo[4] = (byte)0xA0;
        xbeeTwo[5] = (byte)0x3C;
        xbeeTwo[6] = (byte)0x4D;
        xbeeTwo[7] = (byte)0x93;
        xbeeTwo[8] = (byte)0xC7;
        
        byte[] xbeeThree = new byte[9];
        xbeeThree[0] = (byte)0x13;
        xbeeThree[1] = (byte)0xA2;
        xbeeThree[2] = (byte)0x00;
        xbeeThree[3] = (byte)0x40;
        xbeeThree[4] = (byte)0xA0;
        xbeeThree[5] = (byte)0x3C;
        xbeeThree[6] = (byte)0x70;
        xbeeThree[7] = (byte)0xCE;
        xbeeThree[8] = (byte)0xDB;
        
        byte[][] xbees = new byte[3][];
        xbees[0] = xbeeOne;
        xbees[1] = xbeeTwo;
        xbees[2] = xbeeThree;
        
        long ourAddr = RadioFactory.getRadioPolicyManager().getIEEEAddress();
        System.out.println("Our radio address = " + IEEEAddress.toDottedHex(ourAddr));

        ISwitch sw2 = (ISwitch) Resources.lookup(ISwitch.class, "SW2");
        ISwitch sw1 = (ISwitch) Resources.lookup(ISwitch.class, "SW1");
        eDemo.initUART(9600, 8, 0, 1);
        while (true)
        {
            // byte[] buffer;
            String returnString;
            String returnStringHex;
            
            
            for (int ii = 0 ; ii <  3; ii++)
            {
                /* Send a message, check query all at once */
                returnString = "";
                returnStringHex = "";
                uartSender(xbees[ii]);
                 
                byte[] buffer = new byte[64];
                try{
                    if (eDemo.availableUART()>1)
                    {
                        eDemo.readUART(buffer, 0, buffer.length);
                       returnString = returnString + new String(buffer, "US-ASCII");
                        
                        returnStringHex = toHexString(returnString);

    //                    System.out.println(returnStringHex);
                        System.out.println("Confirm Message - " + returnStringHex + "end");
                        
          
                      
                        for (int i = 0; i < 4; i++) {
                        leds.getLED(i).setRGB(100, 0, 0);
                        leds.getLED(i).setOn();
                        }
                    }
                }catch(IOException ex){
                    ex.printStackTrace();
                }
              //  Utils.sleep(1000);
                /* Now get the actual distance */
                uartSender2();
                
                buffer = new byte[20];
                
                returnString = "";
                returnStringHex = "";
                try{
                    if (eDemo.availableUART()>1)
                    {
                        eDemo.readUART(buffer, 0, buffer.length);
                        returnString = returnString + new String(buffer,"US-ASCII").trim();
                        returnStringHex = toHexString(returnString);

    //                    System.out.println(returnStringHex);
                        System.out.println("Distance to xBee" + ii + " - " + returnStringHex);
                        rssi[ii]=buffer[7]*256+buffer[8];
                        if (ii==0)
                        {
                            distance[ii]=(float)(rssi[ii]*0.044848+41.6);
                        }
                        else if (ii==1)
                        {
                            //distance[ii]=rssi[ii]*rssi[ii]*rssi[ii]*(4.4E-10)+rssi[ii]*rssi[ii]*(4E-6)+rssi[ii]*0.018+34;
                            distance[ii]=(float)(rssi[ii]*0.044848+41.6);
                        }
                        else if (ii==2)
                        {
                            distance[ii]=(float)(rssi[ii]*0.046357+41.8);
                        }
                        //distance[ii]=(float)(rssi[ii]*0.044848+41.6);
                        //distance[ii]=(float) (rssi[ii]*rssi[ii]*rssi[ii]*(4.35E-10)-rssi[ii]*rssi[ii]*(4E-6)+rssi[ii]*0.018+34);
                        System.out.println(ii+":"+rssi[ii]);
                        for (int i = 0; i < 4; i++) {
                        leds.getLED(i).setRGB(100, 0, 0);
                        leds.getLED(i).setOff();

                        }
                    }
                }catch(IOException ex){
                    ex.printStackTrace();
                }
                Utils.sleep(25);
            }
            //Utils.sleep(2000);
            
            try {
            // Open up a broadcast connection to the host port
            // where the 'on Desktop' portion of this demo is listening
            rCon = (RadiogramConnection) Connector.open("radiogram://broadcast:" + HOST_PORT);
            dg = rCon.newDatagram(50);  // only sending 12 bytes of data
        } catch (Exception e) {
            System.err.println("Caught " + e + " in connection initialization.");
            notifyDestroyed();
        }
        
//        try
//            {
//                dg.reset();               
//                dg.writeInt(rssi[0]);               
//                dg.writeInt(rssi[1]);              
//                dg.writeInt(rssi[2]);
//                rCon.send(dg);
//            }
          try
            {
                dg.reset();               
                dg.writeFloat(distance[0]);               
                dg.writeFloat(distance[1]);              
                dg.writeFloat(distance[2]);
                rCon.send(dg);
            }
            catch (Exception e)
            {
                System.err.println("Caught " + e + " while attempting to send datagram.");
            }
            Utils.sleep(5000);
            if (false)
                break;
        }
//        while(true){
//        if (sw2.isClosed()) {                  // done when switch is pressed
//            uartSender();
////            Utils.sleep(1000);                  // wait 1 second
//        }
//         if (sw1.isClosed()) {                  // done when switch is pressed
//            uartSender2();
////            Utils.sleep(1000);                  // wait 1 second
//        }
//        
//        byte[] buffer = new byte[20];
//        String returnString = "";
//        String returnStringg ="";
//        try{
//        if(eDemo.availableUART()>1){
//            eDemo.readUART(buffer, 0, buffer.length);
//            returnString = returnString + new String(buffer,"US-ASCII").trim();
//            returnStringg = toHexString(returnString);
////             for(int i=0;i<19;i++){
////                    System.out.println(Integer.toBinaryString(buffer[i]));
////             }
//           
//          
//      
////            System.out.println(returnString);
//            System.out.println(returnStringg);
//            //System.out.println(buffer);
//            for (int i = 0; i < 4; i++) {
//            leds.getLED(i).setRGB(100, 0, 0);
//            leds.getLED(i).setOn();
//            }
////            Utils.sleep(100);
//            for (int i = 0; i < 4; i++) {
//                leds.getLED(i).setOff(); 
//            }
//        }
//        }catch(IOException ex){
//            ex.printStackTrace();
//        }
////        Utils.sleep(1000);
//        }
        
        //sending RSSI to basestation
        
        
        
        
    }
    public void uartSender(byte[] address){
        byte[] snd = new byte[19];
        snd[0] = (byte)0x7E;    //start of api 
        snd[1] = (byte)0x00;    //msb of length
        snd[2] = (byte)0x0F;    //lsb of length
        snd[3] = (byte)0x10;    // api frame for transmit
        snd[4] = (byte)0x01;    // ack
        snd[5] = (byte)0x00;    // 64-bit addr coordinator 
        snd[6] = address[0];
        snd[7] = address[1];
        snd[8] = address[2];
        snd[9] = address[3];
        snd[10] = address[4];
        snd[11] = address[5];
        snd[12] = address[6];
        snd[13] = address[7]; // 16 Bit Address of the xbee
        snd[14] = address[8];
        snd[15] = (byte)0x00;
        snd[16] = (byte)0x00;
        snd[17] = (byte)0x22;

       byte sum = 0;
        for (int ii=3; ii<18;ii++){
            sum +=snd[ii];
        }
 //       byte sumByte = (byte) (0xFF & sum);
        byte chexum = (byte)(0xff-sum);
        snd[18] = chexum;
            
        eDemo.writeUART(snd);
        
        for (int i = 4; i < 8; i++) {
            leds.getLED(i).setRGB(0, 100, 0);
            leds.getLED(i).setOn();
        }
        Utils.sleep(25);
        for (int i = 4; i < 8; i++) {
            leds.getLED(i).setOff(); 
        }
            
    }
    
    
    
    protected void pauseApp() {
        // This is not currently called by the Squawk VM
    }

    /**
     * Called if the MIDlet is terminated by the system.
     * It is not called if MIDlet.notifyDestroyed() was called.
     *
     * @param unconditional If true the MIDlet must cleanup and release all resources.
     */
    protected void destroyApp(boolean unconditional) throws MIDletStateChangeException {
    }

    /**
     * 
     * Convert ASCII charactors to hex nembers, to check the packet deliveries.
     * 
     */
    public static String toHexString(String s) 
    { 
        String str=""; 
        for (int i=0;i<s.length();i++) 
        { 
            int ch = (int)s.charAt(i); 
            String s4 = Integer.toHexString(ch); 
            str = str + " " + s4; 
        } 
        return str; 
    } 
      public void uartSender2(){
        byte[] snd = new byte[8];
        snd[0] = (byte)0x7E;    //start of api 
        snd[1] = (byte)0x00;    //msb of length
        snd[2] = (byte)0x04;    //lsb of length
        snd[3] = (byte)0x08;    // api frame for transmit
        snd[4] = (byte)0x52;    // ack
        snd[5] = (byte)0x44;    // 64-bit addr coordinator 
        snd[6] = (byte)0x42;
        snd[7] = (byte)0x1F;
//        snd[8] = (byte)0x00;
//        snd[9] = (byte)0x2F;
//        snd[10] = (byte)0x70;
//        snd[11] = (byte)0xFF;
//        snd[12] = (byte)0xFF;
//        snd[13] = (byte)0xFF;   //16-bit
//        snd[14] = (byte)0xFE;
//        snd[15] = (byte)0x00;
//        snd[16] = (byte)0x00;
//        snd[17] = (byte)0x22;
//        snd[18] = (byte)0xD1;
        
//        int val = 0;
//        try{
//            val = light.getValue();
//        }catch(IOException ex){}
//        byte b = (byte)(val & 0xff);
//        snd[17] = b;
//        byte sum = 0;
//        for (int ii=3; ii<18;ii++){
//            sum +=snd[ii];
//        }
 //       byte sumByte = (byte) (0xFF & sum);
//        byte chexum = (byte)(0xff-sum);
//        snd[18] = chexum;
//            
        eDemo.writeUART(snd);
        
        for (int i = 4; i < 8; i++) {
            leds.getLED(i).setRGB(0, 100, 0);
            leds.getLED(i).setOn();
        }
        //Utils.sleep(100);
        for (int i = 4; i < 8; i++) {
            leds.getLED(i).setOff(); 
        }
            
    }
}
